﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Lego.Ev3.Android;
using Lego.Ev3.Core;
using Environment = System.Environment;

namespace Lego.Ev3.Tester.Android
{
	[Activity(Label = "Programeer")]
	public class ProgrammeerActivity : Activity
	{
		private Brick _brick;
		private TextView _output;
		private TextView _ports;
		private StringBuilder _portText = new StringBuilder();

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.ProgrammeerWindow);

        }

		
	}
}

