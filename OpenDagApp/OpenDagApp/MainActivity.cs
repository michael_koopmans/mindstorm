﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using Lego.Ev3.Android;
using Lego.Ev3.Core;
using System.Text;

namespace OpenDagApp
{
    [Activity(Label = "OpenDagApp", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Landscape)]
    public class MainActivity : Activity
    {
        private Brick _brick;
        private TextView _output;
        private TextView _ports;
        private StringBuilder _portText = new StringBuilder();

        protected override void OnCreate(Bundle bundle)
        {
            //Remove title bar && notification bar
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            // Opgestarte activity opent hier de main(menu) pagina.
            SetContentView(Resource.Layout.Main);
            // Buttons aanmaken en een actie aan koppelen

            _output = FindViewById<TextView>(Resource.Id.output);
            _ports = FindViewById<TextView>(Resource.Id.ports);

            Button programmeren = FindViewById<Button>(Resource.Id.programmeren);
            Button verbinden = FindViewById<Button>(Resource.Id.verbinden);
            Button besturen = FindViewById<Button>(Resource.Id.besturen);
            Button afsluiten = FindViewById<Button>(Resource.Id.afsluiten);


            _brick = new Brick(new BluetoothCommunication());
            //_brick = new Brick(new NetworkCommunication("192.168.2.237"));
            _brick.BrickChanged += brick_BrickChanged;

            programmeren.Click += delegate { StartActivity(typeof(ProgrammeerActivity));};
            verbinden.Click += async delegate 
            {
                try
                {
                    await _brick.ConnectAsync();
                    _output.Text = "Connected";
                }
                catch (Exception ex)
                {
                    AlertDialog.Builder x = new AlertDialog.Builder(this);
                    x.SetMessage(ex.ToString());
                    x.Create().Show();
                }
            };
            besturen.Click += delegate { StartActivity(typeof(BesturenActivity)); };
            afsluiten.Click += delegate { Android.OS.Process.KillProcess(Android.OS.Process.MyPid()); };
            
        }

        private void brick_BrickChanged(object sender, BrickChangedEventArgs e)
        {
            _portText.Clear();

            foreach (Port port in e.Ports.Values)
            {
                _portText.AppendFormat("{0}: {1}, {2}, {3}", port.Name, port.Type, port.Mode, port.SIValue);
                _portText.AppendLine();
            }

            _ports.Text = _portText.ToString();
        }
    }
}

