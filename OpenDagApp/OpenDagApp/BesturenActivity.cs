﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;

namespace OpenDagApp
{
    [Activity(Label = "OpenDagApp", ScreenOrientation = ScreenOrientation.Landscape)]
    public class BesturenActivity : Activity
    {    
        protected override void OnCreate(Bundle bundle)
        {
            //Remove title bar && notification bar
            RequestWindowFeature(WindowFeatures.NoTitle);            
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            // Opgestarte activity opent hier de besturen pagina.         
            SetContentView(Resource.Layout.Besturen);

            
            Button terug = FindViewById<Button>(Resource.Id.terug);


            terug.Click += delegate { Finish(); };           

            
        }
    }
}

